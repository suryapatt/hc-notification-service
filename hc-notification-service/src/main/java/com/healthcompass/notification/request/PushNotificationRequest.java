package com.healthcompass.notification.request;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
public class PushNotificationRequest implements Serializable {
  
	private String phoneNumber;  // user
  	private String messageType;
    private String language;
    private String message;
    private Map<String,String> messageValuesMap;
   
    public PushNotificationRequest() {
		
	}

	
	
}
