package com.healthcompass.notification.request;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
public class PushNotificationMessageRequest implements Serializable {
  
	private String token;  // user
  	private String title;
    private String subject;
    private String body;
    private Double expiry;
    private Boolean collapisble;
    private String collapseKey;
    private Boolean  priority;
    private String deviceType;
    private String linkPage;
    
    private Map<String,String> extraData;
   
    public PushNotificationMessageRequest() {
		
	}

	
	
}
