package com.healthcompass.notification.request;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
public class CleanUpTokenRequest implements Serializable {
  
	
	
	
	private String userName ;
	private String deviceInfo;
  	
   
    public CleanUpTokenRequest() {
		
	}

	
	
}
