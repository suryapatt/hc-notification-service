package com.healthcompass.notification.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
public class InAppNotificationObject implements Serializable {
  
	
	
	
	private String action_url ;
	private String body; 
  	private String message_type;
  	private String message_status;
    private String sender_id;
    private String recipient_id;
   
    public InAppNotificationObject() {
		
	}

	
	
}
