package com.healthcompass.notification;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;


@SpringBootApplication
@EnableConfigurationProperties
public class NotificationsApplication {

	@Value("#{'${cors-allowed-origins}'.split(', ')}")
	private List<String> allowedOrigins;
	
	@Value("${service-account-file-name}")
	private String serviceAccountFileName;
	
	@Value("${project-id}")
	private String projectId;
	
	public static void main(String[] args) {
		SpringApplication.run(NotificationsApplication.class, args);
		
	}
	
	
	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilterRegistration() {

		// http://stackoverflow.com/questions/31724994/spring-data-rest-and-cors
		CorsConfiguration config = new CorsConfiguration();
		//config.setAllowCredentials(true);
		config.setAllowedOrigins(allowedOrigins);
		config.addAllowedOrigin("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("PATCH");
		config.addAllowedHeader("AUTHORIZATION");
		config.addAllowedHeader("X-Requested-With");
		config.addAllowedHeader("X-Auth-Token");
		config.addAllowedHeader("X-CLIENT-ID");
		config.addAllowedHeader("X-CLIENT_SECRET");
		config.addAllowedHeader("Content-Type");
		config.addAllowedHeader("User-tz");
		
		
		

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		final FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}
		
	@Bean
	Datastore dataStore() throws IOException {
		
		GoogleCredentials googleCredentials = GoogleCredentials
	            .fromStream(new ClassPathResource(serviceAccountFileName).getInputStream());
		
		DatastoreOptions datastoreOptions = DatastoreOptions.getDefaultInstance().toBuilder()
			       .setProjectId(projectId)
			        //.setCredentials(GoogleCredentials.getApplicationDefault())
			         .setCredentials(googleCredentials)
			         .build();
		Datastore db = datastoreOptions.getService();
		
			
	   
	    return db;
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate(); 
	}

}
