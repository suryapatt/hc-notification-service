package com.healthcompass.notification.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.ProjectionEntity;
import com.google.cloud.datastore.ProjectionEntityQuery;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StructuredQuery.Filter;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;
import com.healthcompass.notification.model.UserTokenObject;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class FirestoreService {

	 private final Datastore dataStore;
	 
	 public FirestoreService(Datastore dataStore) {
	        this.dataStore = dataStore;
	 }
	 
	 public String saveToken(UserTokenObject userTokenObject) {

		 
		 // The kind for the new entity
		    log.info("create/update user for Token registration: "+userTokenObject.getUserName()+"");
		    String kind = "User";
		    // The name/ID for the new entity
		    String id = userTokenObject.getUserName();
		    // The Cloud Datastore key for the new entity
		    Key userKey = dataStore.newKeyFactory().setKind(kind).newKey(id);

		    // Prepares the new entity
		    Entity userEntity = Entity.newBuilder(userKey)
		         .build();

		    // Saves the entity
		    dataStore.put(userEntity);
		    
		    // The kind for the new entity
		    String tokenKind = "Token";
		    // The name/ID for the new entity
		    String tokenId = userTokenObject.getToken();
		    // The Cloud Datastore key for the new entity
		    Key tokenKey = dataStore.newKeyFactory().setKind(tokenKind).newKey(tokenId);

		    // Prepares the new entity
		    Entity tokenEntity = Entity.newBuilder(tokenKey)
		        .set("userName", userTokenObject.getUserName())
		        .set("deviceName", userTokenObject.getDeviceName())
		        .set("allowNotifications", userTokenObject.isNotificationsEnabled())
		        .set("createdTime", userTokenObject.getCreatedTime().toString())
		         .build();

		    // Saves the entity
		    log.info("creating new Token for user: "+userTokenObject.getUserName()+"\r\n");
		    log.debug("Token:"+userTokenObject.getToken()+"");
		    dataStore.put(tokenEntity);

		
		
		
	      

	        return "success";
	    }
	 
public String getToken(String token)  {

			//Retrieve entity
    
    
		 // The kind for the new entity
		    String kind = "Token";
		    // The name/ID for the new entity
		    String name = token;
		    // The Cloud Datastore key for the new entity
		    Key tokenKey = dataStore.newKeyFactory().setKind(kind).newKey(name);
		    Entity tokenEntity = dataStore.get(tokenKey);
		    return tokenEntity.getKey().getName();
		  
		    // Prepares the new entity
		
		
		
		
	      

	       
	    }

public void getAllTokensInformation(String userName)  {

	//Retrieve entity
    
    
		 // The kind for the new entity
		    String kind = "Token";
		    // The name/ID for the new entity
		    
		    
		    // get all the tokens for the user
		    Query<Entity> query = Query.newEntityQueryBuilder()
		    	    .setKind("Token")
		    	    .setFilter(PropertyFilter.eq("userName", userName))
		    	    .build();
		    QueryResults<Entity> tokens = dataStore.run(query);
		    while(tokens.hasNext()) {
		    	Entity token = tokens.next();
		    	String deviceToken = token.getKey().getName();
		    }
		    
		    // Prepares the new entity
		
		
		
		
	      

	       
	    }

public QueryResults<Entity> runQuery(Query query){
	
	return dataStore.run(query);
}

public Entity getEntity(String kind, String id) {
	
	  Key kindKey = dataStore.newKeyFactory()
			    .setKind(kind)
			    .newKey(id);
	  
	  return dataStore.get(kindKey);
}

public QueryResults<ProjectionEntity> runProjectionQuery(ProjectionEntityQuery query){
	
	return dataStore.run(query);
}

public String cleanUpToken(String user,String deviceName) {

	 
	 // The kind for the new entity
	    String kind = "User";
	    // The name/ID for the new entity
	    String id = user;
	    // The Cloud Datastore key for the new entity
	    Key userKey = dataStore.newKeyFactory().setKind(kind).newKey(id);

	    // Prepares the new entity
	    Entity userEntity = Entity.newBuilder(userKey)
	         .build();

	    // Saves the entity
	    dataStore.put(userEntity);
	    
	    // The kind for the new entity
	    
	    String tokenKind = "Token";
	    // The name/ID for the new entity
	    String token="";//  get the token corresponding to device here
	    String tokenId = token;
	    // The Cloud Datastore key for the new entity
	    Key tokenKey = dataStore.newKeyFactory().setKind(tokenKind).newKey(tokenId);

	    // Prepares the new entity
	    Entity tokenEntity = Entity.newBuilder(tokenKey)
	        .set("user", id)
	        .set("userConsent", "yes")
	        .build();

	    // Saves the entity
	    dataStore.put(tokenEntity);

	
	
	
    

      return "success";
  }

  public Key getKey(String kind,String identifier) {
	  return dataStore.newKeyFactory().setKind(kind).newKey(identifier);
  }
  
  public String saveEntity(Entity entity) {
	  String returnValue = "success";
	  try {
		  dataStore.put(entity);
		  
	  }catch(Exception e) {
		  e.printStackTrace();
		  returnValue="error";
	  }
	  return returnValue;
  }

}
