package com.healthcompass.notification.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.auth.oauth2.ServiceAccountCredentials;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class GoogleJWTokenGenerator {
	
	@Value("${external-api-service-jwt-auth-file-name}")
	private String externalApiServiceJwtAuthFileName;
	
	@Value("${external-api-service-jwt-auth-email}")
	private String externalApiServiceJwtAuthEmail;
	
	@Value("${external-api-service-jwt-auth-audiences}")
	private String externalApiServiceJwtAuthAudiences;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	public  String GenerateJWTToken() {
		String token="";
		// TODO Auto-generated method stub
		try {
			String saKeyfile=externalApiServiceJwtAuthFileName;
			String saEmail = externalApiServiceJwtAuthEmail;
			String audiences = externalApiServiceJwtAuthAudiences;
			int expiryLength=86400;
			if(saKeyfile != null && !saKeyfile.isEmpty() && 
					saEmail != null && !saEmail.isEmpty() && 
							audiences != null && !audiences.isEmpty()) {
				token = generateJwt(saKeyfile,saEmail,audiences,expiryLength);
			}
			
			System.out.println("JWT Token::"+token+"::");
			
			// following logig token expiry is not needed for api services as client, only needed for web application as client
			/*boolean isValid = false;
			// get the token from session and check if the expiry is in next 30 seconds. If it expires in 30 seconds then 
			//then create a new token
			DecodedJWT jwt = JWT.decode(token);
			long jwtExpiresAt = jwt.getExpiresAt().getTime() / 1000;
			long difference = jwtExpiresAt - (System.currentTimeMillis() / 1000);
			if (difference >= 30) {
				isValid = true;
			}*/
		
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return token;

	}
	
	
	
	// [START endpoints_generate_jwt_sa]
	  /**
	   * Generates a signed JSON Web Token using a Google API Service Account
	   * utilizes com.auth0.jwt.
	   */
	  public String generateJwt(final String saKeyfile, final String saEmail,
	      final String audiences, final int expiryLength)
	      throws FileNotFoundException, IOException {

	    Date now = new Date();
	    Date expTime = new Date(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(expiryLength));

	    // Build the JWT payload
	    JWTCreator.Builder token = JWT.create()
	        .withIssuedAt(now)
	        // Expires after 'expiryLength' seconds
	        .withExpiresAt(expTime)
	        // Must match 'issuer' in the security configuration in your
	        // swagger spec (e.g. service account email)
	        .withIssuer(saEmail)
	        // Must be either your Endpoints service name, or match the value
	        // specified as the 'x-google-audience' in the OpenAPI document
	        .withAudience(audiences)
	        // Subject and email should match the service account's email
	        .withSubject(saEmail)
	        .withClaim("email", saEmail);

	    // Sign the JWT with a service account
	    Resource resource = resourceLoader.getResource("classpath:"+saKeyfile+"");
        InputStream stream = resource.getInputStream(); 
	    //FileInputStream stream = new FileInputStream(new ClassPathResource(saKeyfile).getFile());
	    ServiceAccountCredentials cred = ServiceAccountCredentials.fromStream(stream);
	    RSAPrivateKey key = (RSAPrivateKey) cred.getPrivateKey();
	    
	    
	    Algorithm algorithm = Algorithm.RSA256(null, key);
	    return token.sign(algorithm);
	  }
	  // [END endpoints_generate_jwt_sa]


	  // [START endpoints_jwt_request]
	  /**
	   * Makes an authorized request to the endpoint.
	   */
	  public void checkAndAddAuthorizationHeader(HttpHeaders headers)
	      throws IOException, ProtocolException {
		  
		  String jwtToken=GenerateJWTToken();
		    if(jwtToken != null && !jwtToken.isEmpty()) {
		    	headers.add("Authorization", "Bearer " + jwtToken);
		    }
	    
	  }
	  // [END endpoints_jwt_request]

}
