package com.healthcompass.notification.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import com.google.cloud.Timestamp;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DateTimeUtility {
	
	public static long calculateTimeDuration(Date startTime) {
		    Date endTime = new Date();
		   	long diff = endTime.getTime() - startTime.getTime() ;
        	long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        	return seconds;
        	
		
	}

}
