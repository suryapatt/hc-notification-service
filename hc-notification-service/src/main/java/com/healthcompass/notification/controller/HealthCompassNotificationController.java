package com.healthcompass.notification.controller;


import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.batch.BatchDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.Timestamp;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.ProjectionEntity;
import com.google.cloud.datastore.ProjectionEntityQuery;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.StructuredQuery.CompositeFilter;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;
import com.healthcompass.notification.model.CleanUpTokenObject;
import com.healthcompass.notification.model.EmailNotificationObject;
import com.healthcompass.notification.model.InAppNotificationObject;
import com.healthcompass.notification.model.SmsNotificationObject;
import com.healthcompass.notification.model.UserTokenObject;
import com.healthcompass.notification.request.CleanUpTokenRequest;
import com.healthcompass.notification.request.InAppNotificationServiceRequest;
import com.healthcompass.notification.request.NotificationRequest;
import com.healthcompass.notification.request.PushNotificationMessageRequest;
import com.healthcompass.notification.request.PushNotificationRequest;
import com.healthcompass.notification.request.RegisterPushNotificationTokenRequest;
import com.healthcompass.notification.request.SmsServiceRequest;
import com.healthcompass.notification.service.FirestoreService;
import com.healthcompass.notification.util.DateTimeUtility;
import com.healthcompass.notification.util.GoogleJWTokenGenerator;

import lombok.extern.slf4j.Slf4j;

import com.healthcompass.notification.model.PushNotificationObject;
import com.google.cloud.datastore.QueryResults;





@BasePathAwareController
@Slf4j
public class HealthCompassNotificationController {
  
	@Value("${push-notification-service-url}")
	private String pushNotificationServiceURL;
	
	@Value("${inapp-notification-service-url}")
	private String inAppNotificationServiceURL;
	
	@Value("${sms-service-url}")
	private String smsNotificationServiceURL;
	
	@Value("${email-service-url}")
	private String emailNotificationServiceURL;
	
	@Value("${notification-service-timezone}")
	private String notificationServiceTimeZone;
    
    @Value("${notification-service-language}")
	private String notificationServiceLanguage;
    
    @Value("${notification-service-default-date-format}")
    private String notificationServiceDefaultDateFormat;
	
    	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired 
	FirestoreService firestoreService;
	
	@Autowired
	GoogleJWTokenGenerator googleJWTokenGenerator;
	
	@RequestMapping(value = "/push-notification", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	
	@ResponseBody
	public String pushNotification(@RequestBody PushNotificationRequest pushNotificationRequest) {
	
	// get 	msg, user details from Firestore
		
	String pushNotificationResponseAsJsonStr = null;
	try {
		restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	   
		
		// get all the tokens associated with this user (phone number)
		Query<Entity> query = Query.newEntityQueryBuilder()
		    		    .setKind("Token")
		    		    .setFilter(CompositeFilter.and(
		    		        PropertyFilter.eq("phoneNumber", pushNotificationRequest.getPhoneNumber()), PropertyFilter.eq("allowNotifications", true)))
		    		      .build();
		    	QueryResults<Entity> tokens = firestoreService.runQuery(query);
		        boolean isMessageConstructed=false;
		        
		        // Iterate each token and send push notification request if the notifications are enabled for that token
		    	while (tokens.hasNext()) {
		    		  Entity token = tokens.next();
		    		  Boolean isSendPushNotification = (Boolean) token.getValue("allowNotifications").get();
		    		  if(isSendPushNotification) { // if notifications allowed
		    			  
		    			  String msgTemplate = null;
		    			  Entity message = null;
		    			  PushNotificationMessageRequest pushNotificationMsgRequest = null;
		    			  // translate the message template into the given language. We need to do only once for numerous tokens
		    			  if(!isMessageConstructed) {
		    				  
		    				  message = firestoreService.getEntity("NotificationMessage", pushNotificationRequest.getMessage()+"_"+pushNotificationRequest.getLanguage());
		    				  msgTemplate = (String) message.getValue("MessageTemplate").get();
		    				  Map<String,String> msgValuesMap = pushNotificationRequest.getMessageValuesMap();
		    				  for(String key : msgValuesMap.keySet() ) {
		    					  String value = (String) msgValuesMap.get(key);
		    					  msgTemplate=msgTemplate.replaceAll("<"+key+".", value);
		    				  }
		    				  log.debug((String) message.getValue("MessageType").get());
		    				  log.debug((String) message.getValue("priority").get());
		    				  log.debug((String) message.getValue("isCollapisble").get());
		    				  log.debug((String) message.getValue("link_page").get());
		    				  log.debug((String) message.getValue("MessageTemplate").get());
				    		 
		    			  }
		    			  
		    			  // populate the push notification message request from token and message objects
		    			   pushNotificationMsgRequest = new PushNotificationMessageRequest();
		    			  
		    			  // set Token specific attributes
		    			  pushNotificationMsgRequest.setToken(token.getKey().getName()); 
		    			  pushNotificationMsgRequest.setDeviceType((String) token.getValue("deviceName").get()); 
		    			  
		    			  
		    			  // set message specific attributes
		    			  pushNotificationMsgRequest.setBody(msgTemplate);
		    			  pushNotificationMsgRequest.setSubject((String) message.getValue("MessageType").get()); 
		    			  pushNotificationMsgRequest.setTitle((String) message.getValue("MessageType").get()); 
		    			  pushNotificationMsgRequest.setCollapisble((Boolean) message.getValue("isCollapisble").get()); 
		    			  pushNotificationMsgRequest.setPriority((Boolean) message.getValue("priority").get());
		    			  pushNotificationMsgRequest.setLinkPage((String) message.getValue("link_page").get());
		    			  pushNotificationMsgRequest.setExtraData(new HashMap<String,String>());
		    			  
		    			  // call Push Notifications API
		    			  HttpEntity<PushNotificationMessageRequest> request = 
		    		    	      new HttpEntity<PushNotificationMessageRequest>(pushNotificationMsgRequest, headers);
		    		    ObjectMapper objectMapper = new ObjectMapper();	    
		    		    	    pushNotificationResponseAsJsonStr = 
		    		    	      restTemplate.postForObject(pushNotificationServiceURL+"/send-push-notification", request, String.class);
		    			  
		    		  }// end for a token
		    		  // Go to next token and send push message
		    		 
		    		 
		    	}
		
		
		
	  
	    	   	    	  
	    
	} catch(Exception e) {
		e.printStackTrace();
		// Logger Service
	}
     return pushNotificationResponseAsJsonStr;
	}
	
	@RequestMapping(value = "/register-push-notification-token", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	
	@ResponseBody
	public String registerToken(@RequestBody RegisterPushNotificationTokenRequest token) {
		log.info("Received request to register the push notification token\r\n");
		log.debug(token.toString());
		String registerTokenResponseAsJsonStr = "success";
		
				
		// store the token against user (device) using Firestore Service
		
		UserTokenObject userTokenObject = new UserTokenObject();
		userTokenObject.setUserName(token.getUserName());
		userTokenObject.setDeviceName(token.getDeviceInfo());
		userTokenObject.setToken(token.getToken());
		userTokenObject.setNotificationsEnabled(true);
		userTokenObject.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		firestoreService.saveToken(userTokenObject);
		log.info("Successfully saved the Token\r\n");
		log.debug(token.toString());
	    return registerTokenResponseAsJsonStr;
	}
	
	@RequestMapping("/sms-notification")
	@ResponseBody
	public String smsNotification(@RequestBody SmsServiceRequest smsRequest) {
		SmsNotificationObject notifObj = new  SmsNotificationObject();
		notifObj.setPhoneNumber(smsRequest.getPhoneNumber());
		notifObj.setMessage(smsRequest.getMessage());
		 String smsServiceResponseAsJsonStr = null;
		try {
			restTemplate = new RestTemplate();
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    
		    HttpEntity<SmsNotificationObject> request = 
		    	      new HttpEntity<SmsNotificationObject>(notifObj, headers);
		    ObjectMapper objectMapper = new ObjectMapper();	    
		    	     smsServiceResponseAsJsonStr = 
		    	      restTemplate.postForObject(smsNotificationServiceURL, request, String.class);
		    	    
		    
		}catch(Exception e) {
			e.printStackTrace();
			// Logger Service
		}
		 
		 return smsServiceResponseAsJsonStr;    	
	}
	
	@RequestMapping("/inapp-notification")
	@ResponseBody
	public String inAppNotification(@RequestBody InAppNotificationServiceRequest inAppNotificationRequest) {
		
		InAppNotificationObject notifObj = new  InAppNotificationObject();
		notifObj.setAction_url(inAppNotificationServiceURL);
		notifObj.setBody(inAppNotificationRequest.getBody());
		notifObj.setMessage_type("INAPP");
		notifObj.setMessage_status("New");
		notifObj.setSender_id(inAppNotificationRequest.getSender_id());
		notifObj.setRecipient_id(inAppNotificationRequest.getRecipient_id());
		String inAppNotificationResponseAsJsonStr = null;
		try {
			restTemplate = new RestTemplate();
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    
		    HttpEntity<InAppNotificationObject> request = 
		    	      new HttpEntity<InAppNotificationObject>(notifObj, headers);
		    ObjectMapper objectMapper = new ObjectMapper();	    
		    	     inAppNotificationResponseAsJsonStr = 
		    	      restTemplate.postForObject(inAppNotificationServiceURL, request, String.class);
		    	   	    	  
		    
		}catch(Exception e) {
			e.printStackTrace();
			// Logger Service
		}
		
		return inAppNotificationResponseAsJsonStr;
	   
	}
	
	@RequestMapping("/cleanUpTokensOnLogout")
	@ResponseBody
	public String cleanUpTokensOnLogout(@RequestBody CleanUpTokenRequest cleanUpTokenRequest)  {
		 return firestoreService.cleanUpToken(cleanUpTokenRequest.getUserName(),cleanUpTokenRequest.getDeviceInfo());
		
		   
		}
		
		
	
	
	@RequestMapping(value = "/send-notifications", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String sendNotifications(@RequestBody NotificationRequest notificationRequest,@RequestHeader Map<String, String> headers) {
		
		
		log.info("Received request to send notification for message with Trigger ID:"+notificationRequest.getMessageTriggerId()+"\r\n");
		log.debug(notificationRequest.toString());
		
		String sendNotificcationsResponseAsJsonStr = "success";
		
				
			// get the message_trigger entitiy object
		
		  	String kind = "Message_Triggers";
		    // The name/ID for the new entity
		    int messageTriggerId = notificationRequest.getMessageTriggerId();
		    
		    // get the Message Triger Entity
		   
		    Entity messageTriggerEntity = firestoreService.getEntity(kind, Integer.toString(messageTriggerId));
		    log.info("Retrieved Message Trigger entity with ID:"+messageTriggerId+"");
		 // get distinct recepient types for this message trigger
		    
		    Date now = new Date();
		    log.debug("Retrieving Messages that are associated with Trigger:"+messageTriggerId+"");
		    ProjectionEntityQuery distinceRecepientTypesQuery = Query.newProjectionEntityQueryBuilder()
		    		 						.setKind("Messages")
		    		 						.setDistinctOn("Message_Recepient_Type_Id")
		    		 						.setProjection("Message_Recepient_Type_Id")
				    		       		    .setFilter(PropertyFilter.eq("Message_Trigger_Id", Long.parseLong(messageTriggerEntity.getKey().getName())))
				    		       		    .build();
				    		      
				    	QueryResults<ProjectionEntity> messageRecepientTypes =  firestoreService.runProjectionQuery(distinceRecepientTypesQuery);
		                if(messageRecepientTypes.hasNext()) {
		                	log.debug("Time taken to retrieve Message_Recepients for Message Trigger ID:"+messageTriggerId+" is "+DateTimeUtility.calculateTimeDuration(now)+" seconds ");
		                	log.info("Retrieved Messages for Message Trigger ID:"+messageTriggerId+"");
		                	
		                }else {
		                	log.warn("No Messages are found for Message Trigger ID:"+messageTriggerId+"");
		                	log.info("Nothing to send! Exiting");
		                	return sendNotificcationsResponseAsJsonStr;
		                }
				    	  
		                
				    	while (messageRecepientTypes.hasNext()) {
				    		ProjectionEntity messageRecepientType = messageRecepientTypes.next();
				    		Long recepientId  = (Long) messageRecepientType.getValue("Message_Recepient_Type_Id").get();
				    		log.debug("Retrieving  Messages for Recepient type:"+recepientId+"");
				    		Date start = new Date();
				    		  // get all the possible notifications that are to be sent
				  		    Query<Entity> query = Query.newEntityQueryBuilder()
				  				    		    .setKind("Messages")
				  				    		    .setFilter(CompositeFilter.and(
				  					    		        PropertyFilter.eq("Message_Trigger_Id", Long.parseLong(messageTriggerEntity.getKey().getName())), PropertyFilter.eq("Message_Recepient_Type_Id", recepientId)))
				  					    		      .build();
				  				    		  
				  				    	QueryResults<Entity> messages = firestoreService.runQuery(query);
				  				        
				  				    	if(messages.hasNext()) {
						                	log.debug("Time taken to retrieve Messages for Message Trigger ID:"+messageTriggerId+" and Recepient type:"+recepientId+" is "+DateTimeUtility.calculateTimeDuration(start)+" seconds ");
						                	log.info("Retrieved Messages for Message Trigger ID:"+messageTriggerId+" and Recepient type:"+recepientId+"");
						                }else {
						                	log.warn("No Messages are found for Message Trigger ID:\"+messageTriggerId+\" and Recepient type:"+recepientId+"");
						                	log.info("Ignoring the Recepient and continuing with next recepient type");
						                	continue;
						                }
				  				    	boolean isSMS=false;
				  				        boolean isEmail=false;
				  				        boolean isPush=false;
				  				        
				  				        boolean isPatientRecepient=false;
				  				        boolean isPractitionerReceipient=false;
				  				        boolean isPractitinerNonDocRecepient=false;
				  				        boolean isProviderAdminRecepient=false;
				  				        boolean isAllRecepients=false;
				  				        
				  				        String targetUserName = null;
				  			    		String targetPhoneNumber=null;
				  			    		String targetEmail=null;
				  				        
				  			    		log.debug("Processing Messages one by one for Recepient Type: "+recepientId+"");
				  				    	while (messages.hasNext()) {
				  				    		isSMS=false;
					  				        isEmail=false;
					  				        isPush=false;
					  				        
					  				        isPatientRecepient=false;
					  				        isPractitionerReceipient=false;
					  				        isPractitinerNonDocRecepient=false;
					  				        isProviderAdminRecepient=false;
					  				        isAllRecepients=false;
					  				        
					  				        targetUserName = null;
					  			    		targetPhoneNumber=null;
					  			    		targetEmail=null;
				  				    		boolean sendMessage=false; // we need to check some conditions before we determine to send the message
				  				    		Entity message = messages.next();
				  				    		  
				  				    		  // check if the max number of messages is met
				  				    		Long messageId = (Long) message.getValue("Message_Id").get();
				  				    		Long maxMsgs = (Long) message.getValue("Max_Messages").get(); 
				  				    		log.info("Processing Message with Message ID:"+messageId+" for the Recepient Type: "+recepientId+"");
				  				    		Map<String,String> msgValuesMap = notificationRequest.getMessageKeys();
				  				    		String uniqueIdentifier = (String) msgValuesMap.get("unique_identifier");
				  				    		 
				  				    		Long messageTypeId = (Long) message.getValue("Message_Type_Id").get();
				  				    		  
				  				    		 
				  				    		 
				  				    		  switch (messageTypeId.intValue()) {
				  							case 1:
				  								isSMS=true;
				  								log.debug("Processing SMS Message");
				  								//if(uniqueIdentifier == null && maxMsgs > 1) {
				  								if(uniqueIdentifier == null) {
				  									log.warn("Unique Identifier not present in request! Using phone number ("+(String) msgValuesMap.get("user_phone_number")+") as Unique Identifier. \r\n"
				  											+ "There is a chance of overriding previous message_deliveries information");
				  													  									 
				  								}
				  								uniqueIdentifier= (uniqueIdentifier != null) ? uniqueIdentifier : (String) msgValuesMap.get("user_phone_number");
				  								
				  								
				  								 
				  								break;
				  								
				  							case 2:
				  								isEmail=true;
				  								
				  								
				  								log.debug("Processing Email Message");
				  								//if(uniqueIdentifier == null && maxMsgs > 1) {
				  								if(uniqueIdentifier == null) {
				  									log.warn("Unique Identifier not present in request! Using Email ("+(String) msgValuesMap.get("user_email")+") as Unique Identifier. "
				  											+ "There is a chance of overriding previous message_deliveries information");
				  													  									 
				  								}
				  								uniqueIdentifier = (uniqueIdentifier != null) ? uniqueIdentifier : (String) msgValuesMap.get("user_email");
				  								
				  								
				  								break;
				  								
				  							case 3:
				  								isPush=true;
				  								
				  								log.debug("Processing Push Notification Message");
				  								//if(uniqueIdentifier == null && maxMsgs > 1) {
				  								if(uniqueIdentifier == null ) {
				  									log.warn("Unique Identifier not present in request! Using User name ("+(String) msgValuesMap.get("user_name")+") as Unique Identifier. "
				  											+ "There is a chance of overriding previous message_deliveries information");
				  													  									 
				  								}
				  								uniqueIdentifier = (uniqueIdentifier != null) ? uniqueIdentifier : (String) msgValuesMap.get("user_name");
				  								break;

				  							default:
				  								break;
				  							}
				  				    		
				  				    		 // get all the possible notifications that are to be sent
				  				    		Date starting = new Date() ;
				  				    		log.debug("Retrieving  Message_Deliveries for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+""); 
				  				  		    Query<Entity> maxNumberMsgsQry = Query.newEntityQueryBuilder()
				  				  				    		    .setKind("Message_Deliveries")
				  				  				    		    .setFilter(CompositeFilter.and(
				  				  					    		        PropertyFilter.eq("Message_Trigger_Id", Long.parseLong(messageTriggerEntity.getKey().getName())), 
				  				  					    		        PropertyFilter.eq("Message_Id", messageId),
				  				  					    		        PropertyFilter.eq("Unique_Identifier", uniqueIdentifier+"-"+messageId)))
				  				  					    		      .build();
				  				  				    		  
				  				  				    	QueryResults<Entity> messageDeliveries = firestoreService.runQuery(maxNumberMsgsQry);
				  				  				    	
				  				  				  
				  				  				        Long previousMsgs = 0L;
				  				  				    	if(messageDeliveries.hasNext()) { // means a previous message was already sent for this business case
					  				  				    	log.debug("Time taken to retrieve Message_Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+" is "+DateTimeUtility.calculateTimeDuration(starting)+" seconds ");
					  					                	log.info("Retrieved Message_Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
					  				  				    	
					  					                	Entity messageDelivery = messageDeliveries.next();
					  				  				    	
					  				  				        previousMsgs = (Long) messageDelivery.getValue("Number_Msgs_Sent").get();
					  				  				        log.debug("There were already "+previousMsgs+ " notifications sent with Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
					  				  				        log.debug("Max Notifications for this Unique Identifier:"+uniqueIdentifier+"-"+messageId+" is "+maxMsgs+"");
					  				  				        
					  				  				        if(previousMsgs < maxMsgs) {
					  				  				        	
					  				  				        	
					  				  				            log.info("Number of notificatioins sent:"+previousMsgs+" are less than Max Notifications for this Unique Identifier:"+uniqueIdentifier+"-"+messageId+" is "+maxMsgs+"");
					  				  				        	// send the message as the number of sends is less than max messages
					  				  				        	// check if the frequency of the message is meeting current time condition.
					  				  				            
					  				  				            Integer msgFreq = Integer.parseInt((String) message.getValue("Frequency").get());
					  				                            log.debug(("Checking if the message triggering is due based on its frequency: "+msgFreq+" hours"));
					  				  				        	Timestamp timeStamp = (Timestamp) messageDelivery.getValue("Previous_Sent_Date").get();
					  				  				            //######
						  				  				        						  				  				 	     
						  				  				     	 
						  			  				     	   DateFormat sdf = new SimpleDateFormat(notificationServiceDefaultDateFormat);
						  			  				     	   DateTimeFormatter zoneConvertDateformatter = DateTimeFormatter.ofPattern(notificationServiceDefaultDateFormat+" z");
						  			  				     	   ZonedDateTime dbZonedDateTime = ZonedDateTime.parse(sdf.format(timeStamp.toDate().getTime())+" "+notificationServiceTimeZone+"", zoneConvertDateformatter); // In db, notificationServiceTimeZone
						  			  				 		   ZonedDateTime  previouslySentDate = dbZonedDateTime.withZoneSameInstant(ZoneId.of(notificationServiceTimeZone));
						  			  				 		 
						  			  				 		 
						  			  				           DateTimeFormatter notificationServiceDateFormatter = DateTimeFormatter.ofPattern(notificationServiceDefaultDateFormat); 
						  			  				 	   
						  			  				 	       String previouslySentDateAsString = previouslySentDate.format(notificationServiceDateFormatter);
						  			  				 	      
						  			  				 	       log.debug("Last sent notification date and time:"+previouslySentDateAsString+" at the Timezone:Aisa/Calcutta");
						  			  				        
						  			  				           ZonedDateTime  nextScheduledDate = previouslySentDate.plusHours(msgFreq);
						  			  				           String nextScheduledDateAsString = nextScheduledDate.format(notificationServiceDateFormatter);
						  			  				           log.debug("Next scheduled notification date and time:"+nextScheduledDateAsString+" at the Timezone:Aisa/Calcutta") ;
						  			  				            		  				        
						  			  				        	Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone(notificationServiceTimeZone));
						  			  				            log.debug("Current date and time:"+sdf.format(currentDate.getTime())+" at the Timezone "+notificationServiceTimeZone+"");
						  			  				        	
						  			  				            long diff=0; // millis
						  			  				            long minutes=0; // minutes
						  			  				            try {
						  			  				            	diff = sdf.parse(nextScheduledDateAsString).getTime()-currentDate.getTimeInMillis();
						  				  				        	minutes = TimeUnit.MILLISECONDS.toMinutes(diff); 
						  			  				            }catch(Exception pse) {
						  			  				            	log.error("Parse Error while calculating the time difference between next scheduled date of job and current date"+pse.getMessage());
						  			  				            	pse.printStackTrace();
						  			  				            	diff=0;
						  			  				            	minutes=0;
						  			  				            }
						  			  				        	
						  			  				            log.debug("Time difference between Current date and Next notification schedule is:"+minutes+" minutes");
						  			  				        
						  			  				            
					  				  				             if(-59 <= minutes && minutes <= 59 ) {
					  				  				        		// log.info("Retrieved Message_Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
					  				  				        	    log.info("Message has passed all conditions and ready to be sent: Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+""+" ");
				  				  					                sendMessage=true;
							  				  					     
				  				  					             } else{
				  				  					            	 log.info("Message has not passed all conditions to be sent: Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+""+" ");
				  				  					                 log.info("Skipping the message Delivery the Message: Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+""+" ");
				  				  					                sendMessage=false;
				  				  					             }// end current schedule time
				  				  					         } else{
					  				  				        	
					  				  				        	log.warn("The max number of msgs: "+maxMsgs+ " limit has been already reached for this notification with Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
					  				  				        	log.info("Skipping the message delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
					  				  				            sendMessage=false;
					  				  				         }// current sends < max sends
				  				  				 
							  				    		
				  				  				    	}else {  // This is first instance of sending the message
				  				  				    		// insert the first instance of message delivery
				  				  				    		log.debug("No Message_Deliveries found for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
				  				  				    	    sendMessage=true;
				  				  				    	}
				  				  				    	if(sendMessage) {
				  				  				    		
				  				  				    	 Long messageRecepientId = (Long) message.getValue("Message_Recepient_Type_Id").get();
				  				    		 
							  				    		  switch (messageRecepientId.intValue()) {
							  								case 1:
							  									isPatientRecepient=true;
							  									log.debug("Message is to be sent to Patient");
							  									break;
							  									
							  								case 2:
							  									isPractitionerReceipient=true;
							  									log.debug("Message is to be sent to Practitioner");
							  									break;
							  									
							  								case 3:
							  									isPractitinerNonDocRecepient=true;
							  									log.debug("Message is to be sent to Practitioner Non-Doctor");
							  									break;
							  									
							  								case 4:
							  									isProviderAdminRecepient=true;
							  									log.debug("Message is to be sent to Provider");
							  									break;
							  									
							  								case 5:
							  									isAllRecepients=true;
							  									log.debug("Message is to be sent to all Recepient types");
							  									break;
			
							  								default:
							  									break;
							  								}
							  				    		  
				  				    		  // translate the message with appropriate values
				  				    		  
				  				    		  Entity messageTemplate = firestoreService.getEntity("Message_Templates", message.getKey().getName());
				  		    				  String msgTemplate = (String) messageTemplate.getValue("Message_Text").get();
				  		    				 
				  		    				  for(String key : msgValuesMap.keySet() ) {
				  		    					  String value = (String) msgValuesMap.get(key);
				  		    					  msgTemplate=msgTemplate.replaceAll("<<"+key+">>", value);
				  		    				  }
				  		    				  log.debug("Message after translation:"+msgTemplate+"");
				  				    		  if(isSMS) {
				  				    			  
				  				    			  
				  				    			  if(isPatientRecepient) {
				  				    				  targetPhoneNumber=msgValuesMap.get("patient_phone_number");
				  				    				  if(targetPhoneNumber == null || targetPhoneNumber.isEmpty()) {
				  				    					  log.warn("Patient phone number is empty or null,either the key:patient_phone_number is missing in the message map or its value is null, cannot send SMS!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    				  
				  				    			  }
				  				    			  if(isPractitionerReceipient) {
				  				    				  
				  				    				  targetPhoneNumber=msgValuesMap.get("practitioner_phone_number");
				  				    				  if(targetPhoneNumber == null || targetPhoneNumber.isEmpty()) {
				  				    					  log.warn("Practitioner phone number is empty or null, either the key:practitioner_phone_number is missing in the message map or its value is null, cannot send SMS!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isPractitinerNonDocRecepient) {
				  				    				  targetPhoneNumber=msgValuesMap.get("practitioner_phone_number"); // use correct key later
				  				    				  if(targetPhoneNumber == null || targetPhoneNumber.isEmpty()) {
				  				    					  log.warn("Practioner phone number is empty or null, either the key:practitioner_phone_number is missing in the message map or its value is null, cannot send SMS!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				 
				  				    			  }
				  				    			  if(isProviderAdminRecepient) {
				  				    				  targetPhoneNumber=msgValuesMap.get("provider_admin_phone_number");
				  				    				  if(targetPhoneNumber == null || targetPhoneNumber.isEmpty()) {
				  				    					  log.warn("Provider phone number is empty or null,  either the key:provider_admin_phone_number is missing in the message map or its value is null, cannot send SMS!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isAllRecepients) {
				  				    				  targetPhoneNumber=msgValuesMap.get("user_phone_number");
				  				    				  if(targetPhoneNumber == null || targetPhoneNumber.isEmpty()) {
				  				    					  log.warn("Recepient phone number is empty or null,  either the key:user_phone_number is missing in the message map or its value is null,  cannot send SMS!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  log.debug("Target phone number for SMS:"+targetPhoneNumber);
				  				    			  // send sms
				  				    			  if(targetPhoneNumber != null && !targetPhoneNumber.isEmpty() ) {
				  				    			  
				  					    			  SmsNotificationObject notifObj = new  SmsNotificationObject();
				  					    		       notifObj.setPhoneNumber(targetPhoneNumber);
				  					    			   //notifObj.setPhoneNumber("+919490755290"); 
				  					    			   notifObj.setMessage(msgTemplate);
				  					    			  String smsServiceResponseAsJsonStr = null;
				  					    				try {
				  					    					restTemplate = new RestTemplate();
				  					    				    HttpHeaders smsRequestHeaders = new HttpHeaders();
				  					    				    smsRequestHeaders.setContentType(MediaType.APPLICATION_JSON);
				  					    				    googleJWTokenGenerator.checkAndAddAuthorizationHeader(smsRequestHeaders);
				  					    				    HttpEntity<SmsNotificationObject> request = 
				  					    				    	      new HttpEntity<SmsNotificationObject>(notifObj, smsRequestHeaders);
				  					    				    ObjectMapper objectMapper = new ObjectMapper();	
				  					    				    log.debug("Calling SMS Service for phone number:"+targetPhoneNumber+" and message:"+notifObj.getMessage()+"");
				  					    				    	     smsServiceResponseAsJsonStr = 
				  					    				    	      restTemplate.postForObject(smsNotificationServiceURL, request, String.class);
				  					    				    	   log.info("Returned:"+smsServiceResponseAsJsonStr+" code back from SNS server for phone number:"+targetPhoneNumber+" and message:"+notifObj.getMessage()+"");
				  					    				    	       // The kind for the new entity
								  				  					    String messageDeliveryKind = "Message_Deliveries";
								  				  					    // The name/ID for the new entity
								  				  					  
								  				  					    // The Cloud Datastore key for the new entity
								  				  					    Key messageDeliveryKey = firestoreService.getKey(messageDeliveryKind, uniqueIdentifier+"-"+messageId);
								  				  					    Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone(notificationServiceTimeZone));
								  				  					    //currentDate.setTime(new Date());
								  				  					    // Prepares the new entity
								  				  					    Entity messageDeliveryEntity = Entity.newBuilder(messageDeliveryKey)
								  				  					        .set("Message_Trigger_Id", Long.parseLong(messageTriggerEntity.getKey().getName()))
								  				  					        .set("Message_Id", messageId)
								  				  					        .set("Unique_Identifier", uniqueIdentifier+"-"+messageId)
								  				  					        .set("Number_Msgs_Sent",++previousMsgs)
								  				  					        .set("Previous_Sent_Date", Timestamp.of(currentDate.getTime()))
								  				  					         .build();

								  				  					    // Saves the entity
								  				  					     log.debug("Saving Message Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
								  				  					     firestoreService.saveEntity(messageDeliveryEntity);
								  				  					     log.info("Saved successfully Message Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
				  					    				    
				  					    				}catch(Exception e) {
				  					    					e.printStackTrace();
				  					    					// Logger Service
				  					    				}
				  				    			  }
				  				    			  
				  				    		  } // end sms
				  				    		  

				  				    		  if(isEmail) {
				  				    			  if(isPatientRecepient) {
				  				    				 targetEmail = msgValuesMap.get("patient_email");
				  				    				 if(targetEmail == null || targetEmail.isEmpty()) {
				  				    					  log.warn("Patient email is empty or null,   either the key:patient_email is missing in the message map or its value is null,   cannot send Email!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    				 
				  				    			  }
				  				    			  if(isPractitionerReceipient) {
				  				    				  targetEmail = msgValuesMap.get("practitioner_email");
				  				    				  if(targetEmail == null || targetEmail.isEmpty()) {
				  				    					  log.warn("Practitioner email is empty or null,either the key:practitioner_email is missing in the message map or its value is null,    cannot send Email!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isPractitinerNonDocRecepient) {
				  				    				  targetEmail = msgValuesMap.get("practitioner_email"); // use correct key later
				  				    				  if(targetEmail == null || targetEmail.isEmpty()) {
				  				    					  log.warn("Practitioner email is empty or null,either the key:practitioner_email is missing in the message map or its value is null,    cannot send Email!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isProviderAdminRecepient) {
				  				    				  targetEmail = msgValuesMap.get("provider_admin_email");
				  				    				  if(targetEmail == null || targetEmail.isEmpty()) {
				  				    					  log.warn("Provider email is empty or null,either the key:provider_admin_email is missing in the message map or its value is null,    cannot send Email!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				 
				  				    			  }
				  				    			  if(isAllRecepients) {
				  				    				  targetEmail = msgValuesMap.get("user_email");
				  				    				  if(targetEmail == null || targetEmail.isEmpty()) {
				  				    					  log.warn("Recepient email is empty or null,either the key:user_email is missing in the message map or its value is null,     cannot send Email!, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				 
				  				    			  }
				  				    			  log.debug("Target email for Email service:"+targetEmail);
				  				    			 if(targetEmail != null && !targetEmail.isEmpty() ) {
				  				    				 
				  				    			 }else {
				  				    				targetEmail="surya.pattamatta@healthcompass.io";
				  				    			 }
				  				    			  
				  				    			   if(targetEmail != null && !targetEmail.isEmpty() ) {
				  				    				  
				  				    				  EmailNotificationObject emailNotifObj = new  EmailNotificationObject();
				  				    				  emailNotifObj.setTo(targetEmail);
				  				    				  //emailNotifObj.setTo("surya.pattamatta@healthcompass.io"); 
				  					    			  emailNotifObj.setSubject("HealthCompassEmail"); // make it dynamic later
				  					    			  emailNotifObj.setBody(msgTemplate);
				  					    			  String emailServiceResponseAsJsonStr = null;
				  					    				try {
				  					    					restTemplate = new RestTemplate();
				  					    				    HttpHeaders emailRequestHeaders = new HttpHeaders();
				  					    				    emailRequestHeaders.setContentType(MediaType.APPLICATION_JSON);
				  					    				    googleJWTokenGenerator.checkAndAddAuthorizationHeader(emailRequestHeaders);
				  					    				    HttpEntity<EmailNotificationObject> request = 
				  					    				    	      new HttpEntity<EmailNotificationObject>(emailNotifObj, emailRequestHeaders);
				  					    				    ObjectMapper objectMapper = new ObjectMapper();	
				  					    				    log.debug("Calling Email Service for email:"+targetEmail+" and message:"+emailNotifObj.getBody()+"");
				  					    				    emailServiceResponseAsJsonStr = 
				  					    				    	      restTemplate.postForObject(emailNotificationServiceURL, request, String.class);
				  					    				    log.info("Returned:"+emailServiceResponseAsJsonStr+" code back from Email service for email:"+targetEmail+" and message:"+emailNotifObj.getBody()+"");
				  					    				    
				  					    				// The kind for the new entity
					  				  					    String messageDeliveryKind = "Message_Deliveries";
					  				  					    // The name/ID for the new entity
					  				  					  
					  				  					    // The Cloud Datastore key for the new entity
					  				  					    Key messageDeliveryKey = firestoreService.getKey(messageDeliveryKind, uniqueIdentifier+"-"+messageId);
					  				  					    Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone(notificationServiceTimeZone));
					  				  					    //currentDate.setTime(new Date());
					  				  					    // Prepares the new entity
					  				  					    Entity messageDeliveryEntity = Entity.newBuilder(messageDeliveryKey)
					  				  					        .set("Message_Trigger_Id", Long.parseLong(messageTriggerEntity.getKey().getName()))
					  				  					        .set("Message_Id", messageId)
					  				  					        .set("Unique_Identifier", uniqueIdentifier+"-"+messageId)
					  				  					        .set("Number_Msgs_Sent",++previousMsgs)
					  				  					        .set("Previous_Sent_Date", Timestamp.of(currentDate.getTime()))
					  				  					         .build();

					  				  					    // Saves the entity
					  				  					    log.debug("Saving Message Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
					  				  					    firestoreService.saveEntity(messageDeliveryEntity);	    
					  				  					    log.info("Saved succceffully Message Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
				  					    				    
				  					    				}catch(Exception e) {
				  					    					e.printStackTrace();
				  					    					// Logger Service
				  					    				}
				  				    			  }
				  				    			  
				  				    			  
				  				    			  
				  				    		  } // end email
				  	
				  					    	if(isPush) {
				  					    		if(isPatientRecepient) {
				  				    				 targetUserName=msgValuesMap.get("patient_user_name");  
				  				    				 if(targetUserName == null || targetUserName.isEmpty()) {
				  				    					  log.warn("Patient user name is empty or null,   either the key:patient_user_name is missing in the message map or its value is null,   cannot send to Push Notification service, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isPractitionerReceipient) {
				  				    				  
				  				    				  targetUserName=msgValuesMap.get("practitioner_user_name");
				  				    				  if(targetUserName == null || targetUserName.isEmpty()) {
				  				    					  log.warn("Practitioner user name is empty or null,   either the key:practitioner_user_name is missing in the message map or its value is null,   cannot send to Push Notification service, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isPractitinerNonDocRecepient) {
				  				    				  targetUserName=msgValuesMap.get("practitioner_user_name"); // use correct key later
				  				    				  if(targetUserName == null || targetUserName.isEmpty()) {
				  				    					  log.warn("Practitioner user name is empty or null,   either the key:practitioner_user_name is missing in the message map or its value is null,   cannot send to Push Notification service, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				 
				  				    			  }
				  				    			  if(isProviderAdminRecepient) {
				  				    				  targetUserName=msgValuesMap.get("provider_admin_user_name");
				  				    				  if(targetUserName == null || targetUserName.isEmpty()) {
				  				    					  log.warn("Provider user name is empty or null,   either the key:provider_admin_user_name is missing in the message map or its value is null,   cannot send to Push Notification service, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  if(isAllRecepients) {
				  				    				  targetUserName=msgValuesMap.get("user_name");
				  				    				  if(targetUserName == null || targetUserName.isEmpty()) {
				  				    					  log.warn("Recepient user name is empty or null,   either the key:user_name is missing in the message map or its value is null,   cannot send to Push Notification service, Ignoring the message");
				  				    					  continue; //continue to next nessage
				  				    				  }
				  				    				  
				  				    			  }
				  				    			  log.debug("Target user name for retrieving push notification tokens:"+targetUserName);
				  							 
				  				    			  try {
				  				    					restTemplate = new RestTemplate();
				  				    				    HttpHeaders pushNotificationRequestHeaders = new HttpHeaders();
				  				    				    pushNotificationRequestHeaders.setContentType(MediaType.APPLICATION_JSON);
				  				    				    googleJWTokenGenerator.checkAndAddAuthorizationHeader(pushNotificationRequestHeaders);
				  				    				   // targetUserName = "suryap"; 
				  				    					
				  				    					// get all the tokens associated with this user (user name)
				  				    					Query<Entity> pushNotificationTokensQuery = Query.newEntityQueryBuilder()
				  				    					    		    .setKind("Token")
				  				    					    		    .setFilter(CompositeFilter.and(
				  				    					    		        PropertyFilter.eq("userName", targetUserName), PropertyFilter.eq("allowNotifications", true)))
				  				    					    		      .build();
				  				    							log.debug("Retrieving available Tokens for user with user name:"+targetUserName+"");
				  				    							Date startTime = new Date();
				  				    					    	QueryResults<Entity> pushNotificationTokens = firestoreService.runQuery(pushNotificationTokensQuery);
				  				    					    	if(pushNotificationTokens.hasNext()) {
				  								                	log.debug("Time taken to retrieve available Tokens for user:"+targetUserName+"is "+DateTimeUtility.calculateTimeDuration(startTime)+" seconds ");
				  								                	log.info("Retrieved available Tokens for user:"+targetUserName+"");
				  								                }else {
				  								                	log.warn("No Tokens are found for user:"+targetUserName+"");
				  								                	log.info("Ignoring the message and continuing with next message");
				  								                	continue;
				  								                }
				  				    					    	
				  				    					        
				  				    					        // Iterate each token and send push notification request if the notifications are enabled for that token
				  				    					    	boolean isSendPushToTokensSuccess=false;
				  				    					    	while (pushNotificationTokens.hasNext()) {
				  				    					    		
				  				    					    		  Entity token = pushNotificationTokens.next();
				  				    					    		 
				  				    					    		  Boolean isSendPushNotification = (Boolean) token.getValue("allowNotifications").get();
				  				    					    		  if(isSendPushNotification) { // if notifications allowed
				  				    					    			  try {
					  				    					    			 log.info("Processing Token:"+token+"");
					  				    					    			  
					  				    					    			  // populate the push notification message request from token and message objects
					  				    					    			  PushNotificationMessageRequest pushNotificationMsgRequest = new PushNotificationMessageRequest();
					  				    					    			  
					  				    					    			  // set Token specific attributes
					  				    					    			  pushNotificationMsgRequest.setToken(token.getKey().getName()); 
					  				    					    			  pushNotificationMsgRequest.setDeviceType((String) token.getValue("deviceName").get()); 
					  				    					    			  
					  				    					    			  
					  				    					    			  // set message specific attributes
					  				    					    			  pushNotificationMsgRequest.setBody(msgTemplate);
					  				    					    			  pushNotificationMsgRequest.setSubject("Message from Health Compass"); // add subject for message_trigger entitiy and use that later
					  				    					    			  pushNotificationMsgRequest.setTitle("Message from Health Compass"); // add subject for message_trigger entitiy and use that later
					  				    					    			  
					  				    					    			  String expiryAsString =  (String) message.getValue("Expiry").get();
					  				    					    			  Double expiry=null;
					  				    					    			  if(expiryAsString != null && !expiryAsString.isEmpty()) {
					  				    					    				  expiry = Double.valueOf(expiryAsString);
					  				    					    			  }
					  				    					    			  log.info("Token expiry:"+expiry+"");
					  				    					    			  pushNotificationMsgRequest.setExpiry(expiry);
					  				    					    			  pushNotificationMsgRequest.setCollapisble((Boolean) message.getValue("Collapisble").get()); 
					  				    					    			  pushNotificationMsgRequest.setPriority((Boolean) message.getValue("Priority").get());
					  				    					    			  String uniqueIdentifierAsCollapseKey = (String) notificationRequest.getMessageKeys().get("unique_identifier");
					  				    					    			 
					  				    					    			  //String collapseKey = (String) message.getValue("Collapse_Key").get();
					  				    					    			  if(uniqueIdentifierAsCollapseKey != null && !uniqueIdentifierAsCollapseKey.isEmpty()) {
					  				    					    				 
					  				    					    			  }else {
					  				    					    				uniqueIdentifierAsCollapseKey="";
					  				    					    			  }
					  				    					    			  pushNotificationMsgRequest.setCollapseKey(uniqueIdentifierAsCollapseKey);
					  				    					    			 // pushNotificationMsgRequest.setLinkPage((String) message.getValue("link_page").get());
					  				    					    			  pushNotificationMsgRequest.setExtraData(new HashMap<String,String>());
					  				    					    			  
					  				    					    			  // call Push Notifications API
					  				    					    			  
					  				    					    			 log.debug("Calling Push notification with following request:"+pushNotificationMsgRequest.toString()+"");
					  				    					    			  
					  				    					    			  HttpEntity<PushNotificationMessageRequest> request = 
					  				    					    		    	      new HttpEntity<PushNotificationMessageRequest>(pushNotificationMsgRequest, pushNotificationRequestHeaders);
					  				    					    		    ObjectMapper objectMapper = new ObjectMapper();	
					  				    					    		    log.debug("Calling Push notification with following request:"+pushNotificationMsgRequest.toString()+"");
					  				    					    		    	    String pushNotificationResponseAsJsonStr = 
					  				    					    		    	      restTemplate.postForObject(pushNotificationServiceURL+"/send-push-notification", request, String.class);
					  				    					    		    log.info("Returned:"+pushNotificationResponseAsJsonStr+" code from Push Notification Service");
					  				    					    		    log.debug("For token:"+token+"");
					  				    					    		    isSendPushToTokensSuccess=true;;
				  				    					    			  }catch(Exception pushNotifExcep) {
				  				    					    				  log.error("Error while sending push notification to token:"+pushNotifExcep.getMessage()+"");
				  				    					    				  log.debug("Token:"+token+"");
				  				    					    				  pushNotifExcep.printStackTrace();
				  				    					    				  log.info("Continue sending push to next Token");
				  				    					    				  
				  				    					    				  		
				  				    					    			  }
				  				    					    		    	
						  					    				    
				  				    					    			  
				  				    					    		  }// end for a token
				  				    					    		  // Go to next token and send push message
				  				    					    		 
				  				    					    		 
				  				    					    	}
				  				    					
				  				    					
				  				    					   // The kind for the new entity
				  				    					    	if(isSendPushToTokensSuccess) {
				  				    					    		 String messageDeliveryKind = "Message_Deliveries";
								  				  					    // The name/ID for the new entity
								  				  					  
								  				  					    // The Cloud Datastore key for the new entity
								  				  					    Key messageDeliveryKey = firestoreService.getKey(messageDeliveryKind, uniqueIdentifier+"-"+messageId);
								  				  					    Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone(notificationServiceTimeZone));
								  				  					    //currentDate.setTime(new Date());
								  				  					    // Prepares the new entity
								  				  					    Entity messageDeliveryEntity = Entity.newBuilder(messageDeliveryKey)
								  				  					        .set("Message_Trigger_Id", Long.parseLong(messageTriggerEntity.getKey().getName()))
								  				  					        .set("Message_Id", messageId)
								  				  					        .set("Unique_Identifier", uniqueIdentifier+"-"+messageId)
								  				  					        .set("Number_Msgs_Sent",++previousMsgs)
								  				  					        .set("Previous_Sent_Date", Timestamp.of(currentDate.getTime()))
								  				  					         .build();

								  				  					    // Saves the entity
								  				  					    log.debug("Saving Message Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
								  				  					    firestoreService.saveEntity(messageDeliveryEntity);	    
								  				  					    log.info("Saved succceffully Message Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
								  				  					    	    
						  				    				  
						  				    				    	}// end ifSendPushNotificationSuccess
						  				  					   
				  							  // send push
				  				    			 }catch(Exception firestoreExcep) {
			    					    				  log.error("Error while retrieving push notification tokens:"+firestoreExcep.getMessage()+"");
			    					    				  firestoreExcep.printStackTrace();
			    					    					    					    				  		
			    					    	     }
				  							  
				  						  } // end push
				  	  
				  				    	
				  				    	} // end sendMessage=true;
				  				    } // end while messages		 	 
				  				    	
				  						
				  		     
				  		
				    	} // end send messages for each recepient type	  
				    		  
				    	return sendNotificcationsResponseAsJsonStr;	  
		   
	

	} // end sendNotifications method
	
public static void maintest(String[] args) {
		
		Date starting = new Date() ;
  		log.debug("Retrieving  Message_Deliveries for Message Trigger ID:3,Message ID:5 and Unique Identifier:BasicProfileUpdate-+919618504040-5"); 
		    Query<Entity> maxNumberMsgsQry = Query.newEntityQueryBuilder()
				    		    .setKind("Message_Deliveries")
				    		    .setFilter(CompositeFilter.and(
					    		        PropertyFilter.eq("Message_Trigger_Id", 3), 
					    		        PropertyFilter.eq("Message_Id", 5),
					    		        PropertyFilter.eq("Unique_Identifier", "BasicProfileUpdate-+919618504040-5")))
					    		      .build();
		    GoogleCredentials googleCredentials=null;
		    try {
		     googleCredentials = GoogleCredentials
		            .fromStream(new ClassPathResource("firebase_svc_account.json").getInputStream());
		    }catch(Exception e) {
		    	e.printStackTrace();
		    }
			
			DatastoreOptions datastoreOptions = DatastoreOptions.getDefaultInstance().toBuilder()
				       .setProjectId("health-compass-302720")
				       .setCredentials(googleCredentials)
				        .build();
			Datastore db = datastoreOptions.getService();
				    		
		                FirestoreService firestoreService = new FirestoreService(db);
				    	QueryResults<Entity> messageDeliveries =firestoreService.runQuery(maxNumberMsgsQry);
				    	
				  
				        Long previousMsgs = 0L;
				    	if(messageDeliveries.hasNext()) { // means a previous message was already sent for this business case
	  				    	log.debug("Time taken to retrieve Message_Delivery for Message Trigger ID:3,Message ID:5 and Unique Identifier:BasicProfileUpdate-+919618504040-5 is "+DateTimeUtility.calculateTimeDuration(starting)+" seconds ");
		                	log.info("Retrieved Message_Delivery for Message Trigger ID:3,Message ID:5 and Unique Identifier:BasicProfileUpdate-+919618504040-5");
	  				    	
		                	Entity messageDelivery = messageDeliveries.next();
	  				    	
	  				        previousMsgs = (Long) messageDelivery.getValue("Number_Msgs_Sent").get();
	  				        log.debug("There were already "+previousMsgs+ " notifications sent with Unique Identifier:BasicProfileUpdate-+919618504040-5");
	  				        log.debug("Max Notifications for this Unique Identifier:BasicProfileUpdate-+919618504040-5 is 30");
	  				        
	  				        if(previousMsgs < 30) {
	  				        	
	  				        	
	  				            log.info("Number of notificatioins sent:"+previousMsgs+" are less than Max Notifications for this Unique Identifier:BasicProfileUpdate-+919618504040-5 is 30");
	  				        	// send the message as the number of sends is less than max messages
	  				        	// check if the frequency of the message is meeting current time condition.
	  				            
	  				            Integer msgFreq = 48;
	                            log.debug(("Checking if the message triggering is due based on its frequency: "+msgFreq+" hours"));
	  				        	Timestamp timeStamp = (Timestamp) messageDelivery.getValue("Previous_Sent_Date").get();
	  				            //
	  				        	
	  				        	String userLocalDateFormat = "dd-MM-yyyy HH:mm:ss";
	  				 	     
	  				     	 
	  				     	   DateFormat sdf = new SimpleDateFormat(userLocalDateFormat);
	  				     	   DateTimeFormatter zoneConvertDateformatter = DateTimeFormatter.ofPattern(userLocalDateFormat+" z");
	  				     	   ZonedDateTime dbZonedDateTime = ZonedDateTime.parse(sdf.format(timeStamp.toDate().getTime())+" Asia/Calcutta", zoneConvertDateformatter); // In db, this will be in UTC zone
	  				 		   ZonedDateTime  previouslySentDate = dbZonedDateTime.withZoneSameInstant(ZoneId.of("Asia/Calcutta"));
	  				 		 
	  				 		 
	  				           DateTimeFormatter clientDateFormatter = DateTimeFormatter.ofPattern(userLocalDateFormat); // change this as per client timezone
	  				 	   
	  				 	       String previouslySentDateAsString = previouslySentDate.format(clientDateFormatter);
	  				 	      
	  				        	
					
	  				        
	  				        	//DateFormat df = DateFormat.
	  				        	SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	  				            log.debug("Last sent notification date and time:"+previouslySentDateAsString+" at the Timezone:Aisa/Calcutta");
	  				        
	  				            ZonedDateTime  nextScheduledDate = previouslySentDate.plusHours(msgFreq);
	  				        	String nextScheduledDateAsString = nextScheduledDate.format(clientDateFormatter);
	  				            log.debug("Next scheduled notification date and time:"+nextScheduledDateAsString+" at the Timezone:Aisa/Calcutta") ;
	  				            		  				        
	  				        	Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("Aisa/Calcutta"));
	  				            log.debug("Current date and time:"+sdf.format(currentDate.getTime())+" at the Timezone Aisa/Calcutta");
	  				        	//currentDate.setTime(Timestamp.now().toDate());
	  				            long diff=0;
	  				            long minutes=0;
	  				            try {
	  				            	diff = sdf.parse(nextScheduledDateAsString).getTime()-currentDate.getTimeInMillis();
		  				        	minutes = TimeUnit.MILLISECONDS.toMinutes(diff); 
	  				            }catch(Exception e) {
	  				            	
	  				            }
	  				        	
	  				        	
	  				            log.debug("Time difference between Current date and Next notification schedule is:"+minutes+" minutes");
	  				        
	  				            
	  				        	
	  				             if(minutes <=60 ) {
	  				        		// log.info("Retrieved Message_Delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
	  				        	    //log.info("Message has passed all conditions and ready to be sent: Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+""+" ");
					                boolean sendMessage=true;
			  					     
					              } // end current schedule time
					         } else{
	  				        	
	  				        	log.warn("The max number of msgs: 30 limit has been already reached for this notification with Unique Identifier:BasicProfileUpdate-+919618504040-5");
	  				        	//log.info("Skipping the message delivery for Message Trigger ID:"+messageTriggerId+ ",Message ID:"+messageId+" and Unique Identifier:"+uniqueIdentifier+"-"+messageId+"");
	  				        
	  				         }// current sends < max sends
				    	}
				 
	}
}
